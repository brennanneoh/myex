import re

from pydantic import BaseModel

def to_snake(string):
  return re.sub(r'(?<!^)(?=[A-Z])', '_', string).lower()

class Input(BaseModel):
  date: str
  activityNumber: int
  aerobicModality: str
  duration: int
  intensity: int
  expenditure: int
  stepCount: int
  remarks: str

  class Config:
    allow_population_by_field_name = True
    alias_generator = to_snake

class Item(BaseModel):
  date_number: str
  date: str
  activity_number: int
  aerobic_modality: str
  duration: int
  intensity: int
  expenditure: int
  daily_step_count: int
  remarks: str

class Output(BaseModel):
  pass