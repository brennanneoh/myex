import boto3
from boto3.dynamodb.types import TypeSerializer
from fastapi import FastAPI, Request, status
from mangum import Mangum
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse

from backend.models import Input

app = FastAPI()
db = boto3.resource("dynamodb")
table = db.Table("ExercisePhysical")
serializer = TypeSerializer()

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors(),  # optionally include the errors
                "body": exc.body}),
    )

@app.post("/add")
def add(input: Input):
  input = input.dict(by_alias=True)
  input["date_number"] = f'{input["date"]}-{input["activity_number"]}'
  table.put_item(Item=input)

@app.get("/item/{item_id}")
def read_item(item_id: str):
  resp = table.get_item(Key={"date_number": item_id})
  return resp.get("Item")

@app.get("/view")
def view():
  resp = table.scan()
  return resp.get("Items")

handler = Mangum(app, api_gateway_base_path="/backend")