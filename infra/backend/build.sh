#!/bin/bash
mkdir -p build
pip install \
  . \
  --platform manylinux2014_aarch64 \
  --target=./build \
  --implementation cp \
  --python 3.10 \
  --only-binary=:all: --upgrade
find . | grep -E "(/__pycache__$|\.pyc$|\.pyo$)" | xargs rm -rf
(cd build && zip -qr ../../backend.zip .)