#!/bin/bash
mkdir -p build
pip install \
  . \
  --platform manylinux2014_x86_64 \
  --target=./build \
  --implementation cp \
  --python 3.9 \
  --only-binary=:all: --upgrade
find . | grep -E "(/__pycache__$|\.pyc$|\.pyo$)" | xargs rm -rf
(cd build && zip -qr ../../edge.zip .)