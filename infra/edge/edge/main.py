import re

def handler(event, context):
  request = event["Records"][0]["cf"]["request"]

  with_extension = re.match("(.+)\.[a-zA-Z0-9]{2,5}$", request["uri"])
  if not with_extension:
    request["uri"] = f"{request['uri']}.html"
  return request