resource "aws_dynamodb_table" "exercise_physical" {
  name         = "ExercisePhysical"
  hash_key     = "date_number"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "date_number"
    type = "S"
  }
}