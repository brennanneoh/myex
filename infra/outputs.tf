output "backend_url" {
  value = aws_apigatewayv2_stage.lambda.invoke_url
}

output "cloudfront_domain" {
  value = aws_cloudfront_distribution.myex_distribution.domain_name
}
