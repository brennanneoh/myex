data "aws_iam_policy_document" "lambda_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_myex_lambda_backend" {
  name               = "iam_myex_lambda_backend"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

resource "aws_iam_role_policy_attachment" "basic" {
  role       = aws_iam_role.iam_myex_lambda_backend.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy_document" "inline" {
  statement {
    effect    = "Allow"
    actions   = ["dynamodb:Scan", "dynamodb:PutItem", "dynamodb:GetItem"]
    resources = ["arn:aws:dynamodb:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:table/ExercisePhysical"]
  }
}

resource "aws_iam_policy" "policy" {
  policy = data.aws_iam_policy_document.inline.json
}

resource "aws_iam_role_policy_attachment" "inline" {
  role       = aws_iam_role.iam_myex_lambda_backend.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_lambda_function" "backend" {
  filename         = "backend.zip"
  source_code_hash = filebase64sha256("edge.zip")
  function_name    = "myex_backend"
  handler          = "backend.main.handler"
  role             = aws_iam_role.iam_myex_lambda_backend.arn
  runtime          = "python3.10"
  architectures    = ["arm64"]
}
