data "aws_iam_policy_document" "edge_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_myex_lambda_edge" {
  name               = "iam_myex_lambda_edge"
  assume_role_policy = data.aws_iam_policy_document.edge_assume_role.json
}

resource "aws_iam_policy_attachment" "edge_basic" {
  name       = "edge_basic"
  roles      = [aws_iam_role.iam_myex_lambda_edge.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "edge" {
  filename         = "edge.zip"
  source_code_hash = filebase64sha256("edge.zip")
  function_name    = "myex_edge"
  handler          = "edge.main.handler"
  role             = aws_iam_role.iam_myex_lambda_edge.arn
  runtime          = "python3.9"
  architectures    = ["x86_64"]
  publish          = true
}
