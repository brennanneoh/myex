from openpyxl import load_workbook
import boto3

wb = load_workbook(filename = 'exercise_physical.xlsx', data_only=True)
ws = wb.active

client = boto3.client("dynamodb")

for row in ws.iter_rows(min_row=2, max_col=9):
  date_number = row[0].value
  activity_date = row[1].value.strftime("%Y-%m-%d")
  activity_number = str(row[2].value)
  aerobic_modality = row[3].value

  if not row[4].value:
    aerobic_modality = ""

  duration = str(row[4].value)

  if not row[4].value:
    duration = "0"

  intensity = str(row[5].value)

  if not row[5].value:
    intensity = "0"

  expenditure = str(row[6].value)

  if not row[6].value:
    expenditure = "0"

  step_count = str(row[7].value)

  if not row[7].value:
    step_count = "0"

  remarks = row[8].value

  if not row[8].value:
    remarks = ""


  print(f"Insert {date_number}")
  client.put_item(TableName="ExercisePhysical", Item={
    "date_number": {"S": date_number},
    "date": {"S": activity_date},
    "activity_number": {"N": activity_number},
    "aerobic_modality": {"S": aerobic_modality},
    "duration": {"N": duration},
    "intensity": {"N": intensity},
    "expenditure": {"N": expenditure},
    "step_count": {"N": step_count},
    "remarks": {"S": remarks}
  })