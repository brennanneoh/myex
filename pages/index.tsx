import Link from "next/link";
import useSWR, { Fetcher } from 'swr'

const fetcher: Fetcher<[], string> = (url) => fetch(url).then(r => r.json());

const Styles = {
  tableHeadings:
    "text-sm font-medium text-gray-900 px-6 py-4 text-left border-2",
  tableData: "text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap",
};

const View = () => {
  const { data, error } = useSWR<any[], Error>('https://q0j9uk8my6.execute-api.us-east-1.amazonaws.com/backend/view', fetcher);
  
  //   deleting an item from the table
  const deleteItem = async () => {};

  if (!data) return <div>Loading...</div>

  return (
    <div className="container mx-auto py-10 flex flex-col w-screen h-screen items-center">
      <div className="flex w-2/3 justify-end py-4">
        <Link href={{ pathname: "/add", }} >
          <button
            type="button"
            className="inline-block px-6 py-2.5 mr-2 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
          >
            Add Data
          </button>
        </Link>
      </div>
      <p className="text-3xl">View Data</p>
      <div className="flex flex-col w-2/3 py-10">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full">
                <thead className="border-b">
                  <tr>
                    <th scope="col" className={Styles.tableHeadings}>
                      Date
                    </th>
                    <th scope="col" className={Styles.tableHeadings}>
                      Aerobic Modality
                    </th>
                    <th scope="col" className={Styles.tableHeadings}>
                      Duration (min)
                    </th>
                    <th scope="col" className={Styles.tableHeadings}>
                      Intensity (HR)
                    </th>
                    <th scope="col" className={Styles.tableHeadings}>
                      Expenditure (kcal)
                    </th>
                    <th scope="col" className={Styles.tableHeadings}>
                      Daily step count
                    </th>
                    <th
                      scope="col"
                      className="text-sm font-medium text-gray-900 px-6 py-4 text-center border-2"
                    >
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item) => (
                    <tr className="border-b" key={item.id}>
                      <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        {item.date}
                      </td>
                      <td className={Styles.tableData}>{item.aerobic_modality}</td>
                      <td className={Styles.tableData}>{item.duration}</td>
                      <td className={Styles.tableData}>{item.intensity}</td>
                      <td className={Styles.tableData}>{item.expenditure}</td>
                      <td className={Styles.tableData}>{item.step_count}</td>
                      <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap text-center">
                        <Link
                          href={{
                            pathname: "/updatedata",
                            query: {
                              id: item.id,
                              dateAdded: item.dateAdded,
                              firstName: item.firstName,
                              lastName: item.lastName,
                              city: item.city,
                              phoneNumber: item.phoneNumber,
                            },
                          }}
                        >
                          <button
                            type="button"
                            className="inline-block px-6 py-2.5 mr-2 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                          >
                            Edit
                          </button>
                        </Link>
                        <button
                          type="button"
                          className="inline-block px-6 py-2.5 bg-red-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-red-700 hover:shadow-lg focus:bg-red-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out"
                          onClick={() => deleteItem()}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default View;