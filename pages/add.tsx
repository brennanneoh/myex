const Add = () => {
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    const target = event.target as HTMLFormElement;
    event.preventDefault();
    await fetch("https://q0j9uk8my6.execute-api.us-east-1.amazonaws.com/backend/add", {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        date: target.date.value,
        activityNumber: +target.activityNumber.value,
        aerobicModality: target.aerobicModality.value,
        duration: +target.duration.value,
        intensity: +target.intensity.value,
        expenditure: +target.expenditure.value,
        stepCount: +target.stepCount.value,
        remarks: target.remarks.value
      })
    })
  };

  const styles = {
    inputField:
      "form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none",
  };
  return (
    <>
      <div className="flex flex-col justify-center items-center h-screen">
        <p className="text-3xl mb-20">Add Data</p>
        <div className="block p-6 rounded-lg shadow-lg bg-white w-1/3 justify-self-center">
          <form onSubmit={handleSubmit} id="addData-form">
            <div className="form-group mb-6">
              <label htmlFor="date" className="form-label inline-block mb-2 text-gray-700">
                Date
              </label>
              <input type="date" className={styles.inputField} id="date" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="activityNumber" className="form-label inline-block mb-2 text-gray-700">
                Activity Number
              </label>
              <input type="number" className={styles.inputField} id="activityNumber" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="aerobicModality" className="form-label inline-block mb-2 text-gray-700">
                Aerobic Modality
              </label>
              <input type="text" className={styles.inputField} id="aerobicModality" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="duration" className="form-label inline-block mb-2 text-gray-700">
                Duration (min)
              </label>
              <input type="number" className={styles.inputField} id="duration" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="intensity" className="form-label inline-block mb-2 text-gray-700">
                Intensity (HR)
              </label>
              <input type="number" className={styles.inputField} id="intensity" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="expenditure" className="form-label inline-block mb-2 text-gray-700">
                Expenditure (kcal)
              </label>
              <input type="number" className={styles.inputField} id="expenditure" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="stepCount" className="form-label inline-block mb-2 text-gray-700">
                Daily step count
              </label>
              <input type="number" className={styles.inputField} id="stepCount" />
            </div>
            <div className="form-group mb-6">
              <label htmlFor="remarks" className="form-label inline-block mb-2 text-gray-700">
                Remarks
              </label>
              <input type="text" className={styles.inputField} id="remarks" />
            </div>

            <button type="submit"
              className="
    px-6
    py-2.5
    bg-blue-600
    text-white
    font-medium
    text-xs
    leading-tight
    uppercase
    rounded
    shadow-md
    hover:bg-blue-700 hover:shadow-lg
    focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0
    active:bg-blue-800 active:shadow-lg
    transition
    duration-150
    ease-in-out"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  )
}

export default Add;